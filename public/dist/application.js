'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function () {
  // Init module configuration options
  var applicationModuleName = 'mean';
  var applicationModuleVendorDependencies = ['ngResource', 'ngAnimate', 'ngMessages', 'ui.router', 'ui.bootstrap', 'ui.utils', 'angularFileUpload'];

  // Add a new vertical module
  var registerModule = function (moduleName, dependencies) {
    // Create angular module
    angular.module(moduleName, dependencies || []);

    // Add the module to the AngularJS configuration file
    angular.module(applicationModuleName).requires.push(moduleName);
  };

  return {
    applicationModuleName: applicationModuleName,
    applicationModuleVendorDependencies: applicationModuleVendorDependencies,
    registerModule: registerModule
  };
})();

'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider', '$httpProvider',
  function ($locationProvider, $httpProvider) {
    $locationProvider.html5Mode(true).hashPrefix('!');

    $httpProvider.interceptors.push('authInterceptor');
  }
]);

angular.module(ApplicationConfiguration.applicationModuleName).run(["$rootScope", "$state", "Authentication", function ($rootScope, $state, Authentication) {

  // Check authentication before changing state
  $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
    if (toState.data && toState.data.roles && toState.data.roles.length > 0) {
      var allowed = false;
      toState.data.roles.forEach(function (role) {
        if (Authentication.user.roles !== undefined && Authentication.user.roles.indexOf(role) !== -1) {
          allowed = true;
          return true;
        }
      });

      if (!allowed) {
        event.preventDefault();
        if (Authentication.user !== undefined && typeof Authentication.user === 'object') {
          $state.go('forbidden');
        } else {
          $state.go('authentication.signin').then(function () {
            storePreviousState(toState, toParams);
          });
        }
      }
    }
  });

  // Record previous state
  $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
    storePreviousState(fromState, fromParams);
  });

  // Store previous state
  function storePreviousState(state, params) {
    // only store this state if it shouldn't be ignored 
    if (!state.data || !state.data.ignoreState) {
      $state.previous = {
        state: state,
        params: params,
        href: $state.href(state, params)
      };
    }
  }
}]);

//Then define the init function for starting up the application
angular.element(document).ready(function () {
  //Fixing facebook bug with redirect
  if (window.location.hash && window.location.hash === '#_=_') {
    if (window.history && history.pushState) {
      window.history.pushState('', document.title, window.location.pathname);
    } else {
      // Prevent scrolling by storing the page's current scroll offset
      var scroll = {
        top: document.body.scrollTop,
        left: document.body.scrollLeft
      };
      window.location.hash = '';
      // Restore the scroll offset, should be flicker free
      document.body.scrollTop = scroll.top;
      document.body.scrollLeft = scroll.left;
    }
  }

  //Then init the app
  angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});

'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');
ApplicationConfiguration.registerModule('core.admin', ['core']);
ApplicationConfiguration.registerModule('core.admin.routes', ['ui.router']);

(function (app) {
  'use strict';

  app.registerModule('friendships', ['users', 'wishes']);
}(ApplicationConfiguration));

(function (app) {
  'use strict';

  app.registerModule('front');
}(ApplicationConfiguration));

'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('users', ['core']);
ApplicationConfiguration.registerModule('users.admin', ['core.admin']);
ApplicationConfiguration.registerModule('users.admin.routes', ['core.admin.routes']);

'use strict';

// Use Application`` configuration module to register a new module
ApplicationConfiguration.registerModule('wishes', ['core', 'users']);
'use strict';

angular.module('core.admin').run(['Menus',
  function (Menus) {
    Menus.addMenuItem('topbar', {
      title: 'Admin',
      state: 'admin',
      icon: 'cogs',
      type: 'dropdown',
      roles: ['admin']
    });
  }
]);

'use strict';

// Setting up route
angular.module('core.admin.routes').config(['$stateProvider',
  function ($stateProvider) {
    $stateProvider
      .state('admin', {
        abstract: true,
        url: '/admin',
        template: '<ui-view/>',
        data: {
          roles: ['admin']
        }
      });
  }
]);

'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {

    // Redirect to 404 when route not found
    $urlRouterProvider.otherwise(function ($injector, $location) {
      $injector.get('$state').transitionTo('not-found', null, {
        location: false
      });
    });

    // Home state routing
    $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'modules/core/client/views/home.client.view.html'
    })
    .state('not-found', {
      url: '/not-found',
      templateUrl: 'modules/core/client/views/404.client.view.html',
      data: {
        ignoreState: true
      }
    })
    .state('bad-request', {
      url: '/bad-request',
      templateUrl: 'modules/core/client/views/400.client.view.html',
      data: {
        ignoreState: true
      }
    })
    .state('forbidden', {
      url: '/forbidden',
      templateUrl: 'modules/core/client/views/403.client.view.html',
      data: {
        ignoreState: true
      }
    });
  }
]);

'use strict';

angular.module('core').controller('HeaderController', ['$scope', '$state', 'Authentication', 'Menus',
  function ($scope, $state, Authentication, Menus) {
    // Expose view variables
    $scope.$state = $state;
    $scope.authentication = Authentication;

    // Get the topbar menu
    $scope.menu = Menus.getMenu('topbar');

    // Toggle the menu items
    $scope.isCollapsed = false;
    $scope.toggleCollapsibleMenu = function () {
      $scope.isCollapsed = !$scope.isCollapsed;
    };

    // Collapsing the menu after navigation
    $scope.$on('$stateChangeSuccess', function () {
      $scope.isCollapsed = false;
    });
  }
]);

'use strict';

angular.module('core').controller('HomeController', ['$scope', 'Authentication',
  function ($scope, Authentication) {
    // This provides Authentication context.
    $scope.authentication = Authentication;

    // Some example string
    $scope.helloText = 'Welcome in INSPINIA MEAN.JS Boilerplate';
    $scope.descriptionText = 'It is an application skeleton for a typical MEAN web app. You can use it to quickly bootstrap your project.';

  }
]);

'use strict';

//Directive used to set metisMenu and minimalize button
angular.module('core')
  .directive('sideNavigation', ["$timeout", function ($timeout) {
    return {
      restrict: 'A',
      link: function (scope, element) {
        // Call metsi to build when user signup
        scope.$watch('authentication.user', function () {
          $timeout(function () {
            element.metisMenu();
          });
        });
      }
    };
  }])
  .directive('minimalizaSidebar', ["$timeout", function ($timeout) {
    return {
      restrict: 'A',
      template: '<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="" ng-click="minimalize()"><i class="fa fa-bars"></i></a>',
      controller: ["$scope", "$element", function ($scope, $element) {
        $scope.minimalize = function () {
          angular.element('body').toggleClass('mini-navbar');
          if (!angular.element('body').hasClass('mini-navbar') || angular.element('body').hasClass('body-small')) {
            // Hide menu in order to smoothly turn on when maximize menu
            angular.element('#side-menu').hide();
            // For smoothly turn on menu
            $timeout(function () {
              angular.element('#side-menu').fadeIn(400);
            }, 200);
          } else {
            // Remove all inline style from jquery fadeIn function to reset menu state
            angular.element('#side-menu').removeAttr('style');
          }
        };
      }]
    };
  }]);


'use strict';

/**
 * Edits by Ryan Hutchison
 * Credit: https://github.com/paulyoder/angular-bootstrap-show-errors */

angular.module('core')
  .directive('showErrors', ['$timeout', '$interpolate', function ($timeout, $interpolate) {
    var linkFn = function (scope, el, attrs, formCtrl) {
      var inputEl, inputName, inputNgEl, options, showSuccess, toggleClasses,
        initCheck = false,
        showValidationMessages = false,
        blurred = false;

      options = scope.$eval(attrs.showErrors) || {};
      showSuccess = options.showSuccess || false;
      inputEl = el[0].querySelector('.form-control[name]') || el[0].querySelector('[name]');
      inputNgEl = angular.element(inputEl);
      inputName = $interpolate(inputNgEl.attr('name') || '')(scope);

      if (!inputName) {
        throw 'show-errors element has no child input elements with a \'name\' attribute class';
      }

      var reset = function () {
        return $timeout(function () {
          el.removeClass('has-error');
          el.removeClass('has-success');
          showValidationMessages = false;
        }, 0, false);
      };

      scope.$watch(function () {
        return formCtrl[inputName] && formCtrl[inputName].$invalid;
      }, function (invalid) {
        return toggleClasses(invalid);
      });

      scope.$on('show-errors-check-validity', function (event, name) {
        if (angular.isUndefined(name) || formCtrl.$name === name) {
          initCheck = true;
          showValidationMessages = true;

          return toggleClasses(formCtrl[inputName].$invalid);
        }
      });

      scope.$on('show-errors-reset', function (event, name) {
        if (angular.isUndefined(name) || formCtrl.$name === name) {
          return reset();
        }
      });

      toggleClasses = function (invalid) {
        el.toggleClass('has-error', showValidationMessages && invalid);
        if (showSuccess) {
          return el.toggleClass('has-success', showValidationMessages && !invalid);
        }
      };
    };

    return {
      restrict: 'A',
      require: '^form',
      compile: function (elem, attrs) {
        if (attrs.showErrors.indexOf('skipFormGroupCheck') === -1) {
          if (!(elem.hasClass('form-group') || elem.hasClass('input-group'))) {
            throw 'show-errors element does not have the \'form-group\' or \'input-group\' class';
          }
        }
        return linkFn;
      }
    };
  }]);

'use strict';

angular.module('core').factory('authInterceptor', ['$q', '$injector',
  function ($q, $injector) {
    return {
      responseError: function(rejection) {
        if (!rejection.config.ignoreAuthModule) {
          switch (rejection.status) {
            case 401:
              $injector.get('$state').transitionTo('authentication.signin');
              break;
            case 403:
              $injector.get('$state').transitionTo('forbidden');
              break;
          }
        }
        // otherwise, default behaviour
        return $q.reject(rejection);
      }
    };
  }
]);

'use strict';

//Menu service used for managing  menus
angular.module('core').service('Menus', [
  function () {
    // Define a set of default roles
    this.defaultRoles = ['user', 'admin'];

    // Define the menus object
    this.menus = {};

    // A private function for rendering decision
    var shouldRender = function (user) {
      if (!!~this.roles.indexOf('*')) {
        return true;
      } else {
        if(!user) {
          return false;
        }
        for (var userRoleIndex in user.roles) {
          for (var roleIndex in this.roles) {
            if (this.roles[roleIndex] === user.roles[userRoleIndex]) {
              return true;
            }
          }
        }
      }

      return false;
    };

    // Validate menu existance
    this.validateMenuExistance = function (menuId) {
      if (menuId && menuId.length) {
        if (this.menus[menuId]) {
          return true;
        } else {
          throw new Error('Menu does not exist');
        }
      } else {
        throw new Error('MenuId was not provided');
      }

      return false;
    };

    // Get the menu object by menu id
    this.getMenu = function (menuId) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);

      // Return the menu object
      return this.menus[menuId];
    };

    // Add new menu object by menu id
    this.addMenu = function (menuId, options) {
      options = options || {};

      // Create the new menu
      this.menus[menuId] = {
        roles: options.roles || this.defaultRoles,
        items: options.items || [],
        shouldRender: shouldRender
      };

      // Return the menu object
      return this.menus[menuId];
    };

    // Remove existing menu object by menu id
    this.removeMenu = function (menuId) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);

      // Return the menu object
      delete this.menus[menuId];
    };

    // Add menu item object
    this.addMenuItem = function (menuId, options) {
      options = options || {};

      // Validate that the menu exists
      this.validateMenuExistance(menuId);

      // Push new menu item
      this.menus[menuId].items.push({
        title: options.title || '',
        state: options.state || '',
        type: options.type || 'item',
        class: options.class,
        icon: options.icon,
        roles: ((options.roles === null || typeof options.roles === 'undefined') ? this.defaultRoles : options.roles),
        position: options.position || 0,
        items: [],
        shouldRender: shouldRender
      });

      // Add submenu items
      if (options.items) {
        for (var i in options.items) {
          this.addSubMenuItem(menuId, options.state, options.items[i]);
        }
      }

      // Return the menu object
      return this.menus[menuId];
    };

    // Add submenu item object
    this.addSubMenuItem = function (menuId, parentItemState, options) {
      options = options || {};

      // Validate that the menu exists
      this.validateMenuExistance(menuId);

      // Search for menu item
      for (var itemIndex in this.menus[menuId].items) {
        if (this.menus[menuId].items[itemIndex].state === parentItemState) {
          // Push new submenu item
          this.menus[menuId].items[itemIndex].items.push({
            title: options.title || '',
            state: options.state || '',
            roles: ((options.roles === null || typeof options.roles === 'undefined') ? this.menus[menuId].items[itemIndex].roles : options.roles),
            position: options.position || 0,
            shouldRender: shouldRender
          });
        }
      }

      // Return the menu object
      return this.menus[menuId];
    };

    // Remove existing menu object by menu id
    this.removeMenuItem = function (menuId, menuItemState) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);

      // Search for menu item to remove
      for (var itemIndex in this.menus[menuId].items) {
        if (this.menus[menuId].items[itemIndex].state === menuItemState) {
          this.menus[menuId].items.splice(itemIndex, 1);
        }
      }

      // Return the menu object
      return this.menus[menuId];
    };

    // Remove existing menu object by menu id
    this.removeSubMenuItem = function (menuId, submenuItemState) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);

      // Search for menu item to remove
      for (var itemIndex in this.menus[menuId].items) {
        for (var subitemIndex in this.menus[menuId].items[itemIndex].items) {
          if (this.menus[menuId].items[itemIndex].items[subitemIndex].state === submenuItemState) {
            this.menus[menuId].items[itemIndex].items.splice(subitemIndex, 1);
          }
        }
      }

      // Return the menu object
      return this.menus[menuId];
    };

    //Adding the topbar menu
    this.addMenu('topbar', {
      roles: ['*']
    });
  }
]);

'use strict';

// Create the Socket.io wrapper service
angular.module('core').service('Socket', ['Authentication', '$state', '$timeout',
  function (Authentication, $state, $timeout) {
    // Connect to Socket.io server
    this.connect = function () {
      // Connect only when authenticated
      if (Authentication.user) {
        this.socket = io();
      }
    };
    this.connect();

    // Wrap the Socket.io 'on' method
    this.on = function (eventName, callback) {
      if (this.socket) {
        this.socket.on(eventName, function (data) {
          $timeout(function () {
            callback(data);
          });
        });
      }
    };

    // Wrap the Socket.io 'emit' method
    this.emit = function (eventName, data) {
      if (this.socket) {
        this.socket.emit(eventName, data);
      }
    };

    // Wrap the Socket.io 'removeListener' method
    this.removeListener = function (eventName) {
      if (this.socket) {
        this.socket.removeListener(eventName);
      }
    };
  }
]);

(function () {
  'use strict';

  angular
    .module('friendships')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Set top bar menu items
    Menus.addMenuItem('topbar', {
      title: 'Friends',
      state: 'friends.list',
      icon: 'users',
      roles: ['user', 'admin']
    });
  }
}());

'use strict';

angular.module('friendships').config(routeConfig);

routeConfig.$inject = ['$stateProvider'];

function routeConfig($stateProvider) {
  $stateProvider
    .state('friends', {
      abstract: true,
      url: '/friends',
      template: '<ui-view/>'
    })
    .state('friends.list', {
      url: '',
      templateUrl: 'modules/friendships/client/views/list-friendships.client.view.html',
      controller: 'FriendsListController',
      controllerAs: 'vm'
    })
    .state('friends.search', {
      url: '/search/:query',
      templateUrl: 'modules/friendships/client/views/list-friendships.client.view.html',
      controller: 'FriendsSearchController',
      controllerAs: 'vm',
      data: {
        pageTitle: 'Friends'
      },
      resolve: {
        queryResolve: ['$stateParams', 'FriendshipsService', function ($stateParams, FriendshipsService) {
          return FriendshipsService.search({
            query: $stateParams.query
          }).$promise;
        }]
      }
    })
    .state('user', {
      url: '/user/:userId',
      templateUrl: 'modules/friendships/client/views/profile.client.view.html',
      controller: 'ProfileController',
      controllerAs: 'vm',
      resolve: {
        userResolve: ['$stateParams', 'FriendshipsService', function ($stateParams, FriendshipsService) {
          return FriendshipsService.profile({
            userId: $stateParams.userId
          }).$promise;
        }]
      }
    });
}

(function () {
  'use strict';

  angular
    .module('friendships')
    .controller('FriendsListController', FriendsListController);

  FriendsListController.$inject = ['FriendshipsService', '$state'];

  function FriendsListController(FriendshipsService, $state) {
    var vm = this;

    vm.title = 'Friends';
    vm.empty = 'No friends jet.';
    vm.isSearchResult = false;

    vm.users = FriendshipsService.query();

    vm.search = function () {
      $state.go('friends.search', {
        query: vm.searchQuery
      });
    };

    vm.remove = function (user) {
      FriendshipsService.delete({
        friendId: user._id
      }, function () {
        var index = vm.users.findIndex(function (obj) {
          return user._id === obj._id;
        });
        vm.users.splice(index, 1);
      });
    };
  }
}());

(function () {
  'use strict';

  angular
    .module('friendships')
    .controller('ProfileController', ProfileController);

  ProfileController.$inject = ['FriendshipsService', '$state', 'userResolve', 'Wishes'];

  function ProfileController(FriendshipsService, $state, profile, Wishes) {
    var vm = this;

    vm.profile = profile;

    vm.reserve = function (wish) {
      Wishes.reserve({ wishId: wish._id }, {}, function () {
        wish.reserved = true;
      });
    };

    vm.addToMyList = function (wish) {
      var newWish = { name: wish.name, description: wish.description };
      Wishes.save({}, newWish, function () {
        $state.go('wishes.my');
      }, function () {
        // error
      });
    };

    vm.shouldShowFollowButton = function (friend) {
      if (friend._id !== window.user._id)
        return false;

      return window.user.friends.indexOf(friend) !== -1;
    };
  }
}());

(function () {
  'use strict';

  angular
    .module('friendships')
    .controller('FriendsSearchController', FriendsSearchController);

  FriendsSearchController.$inject = ['queryResolve', '$stateParams', 'FriendshipsService', '$state'];

  function FriendsSearchController(queryResults, $stateParams, FriendshipsService, $state) {
    var vm = this;

    vm.title = 'Search results';
    vm.empty = 'No results.';
    vm.searchQuery = $stateParams.query;
    vm.isSearchResult = true;

    vm.users = queryResults;

    vm.clear = function () {
      $state.go('friends.list');
    };

    vm.onChange = function () {
      if (vm.searchQuery) {
        vm.users = FriendshipsService.search({
          query: vm.searchQuery
        });
      }
    };

    vm.add = function (user) {
      vm.error = '';
      FriendshipsService.save({ friend: user._id }, function () {
        var index = vm.users.findIndex(function (obj) {
          return user._id === obj._id;
        });
        vm.users.splice(index, 1);
      }, function (response) {
        vm.error = response.data.message;
      });
    };
  }
}());

'use strict';

angular
  .module('friendships')
  .factory('FriendshipsService', FriendshipsService);

FriendshipsService.$inject = ['$resource'];

function FriendshipsService($resource) {
  return $resource('api/friendships/:friendId', {
    friendId: '@_id'
  }, {
    search: {
      url: '/api/friendships/search/:query',
      method: 'GET',
      isArray: true
    },
    profile: {
      url: '/api/users/:userId/profile',
      method: 'GET'
    }
  });
}
(function () {
  'use strict';

  angular
    .module('front')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('front', {
        url: '/front',
        templateUrl: '/modules/front/client/views/home.client.view.html',
        controller: 'FrontController',
        controllerAs: 'vm'
      });
  }

}());

(function () {
  'use strict';

  angular
    .module('front')
    .controller('FrontController', FrontController);

  FrontController.$inject = ['Wishes', 'FriendshipsService', '$http'];

  function FrontController(Wishes, FriendshipsService, $http) {
    var vm = this;

    vm.user = window.user || undefined;

    vm.title = 'Wishr';

    vm.initUser = function () {
      Wishes.query(function (wishes) {
        vm.myWishes = wishes;
        vm.myWishesSort();
      });

      Wishes.reserved(function (wishes) {
        vm.reservedWishes = wishes;
      });

      vm.friends = FriendshipsService.query();
    };

    if (vm.user) {
      vm.initUser();
    } else {
      vm.myWishes = [];
    }

    vm.myWishesAdd = function () {
      if (!vm.user) {
        vm.newWish._id = +new Date(); // unique id
        vm.myWishes.push(vm.newWish);
        vm.newWish = {};
        vm.myWishesSort();
      } else {
        Wishes.save(vm.newWish, function (newWish) {
          vm.myWishes.push(newWish);
          vm.myWishesSort();
        });
      }
    };

    vm.myWishesSort = function () {
      vm.myWishes.sort(function (wish1, wish2) {
        return wish1.granted > wish2.granted;
      });
    };

    vm.myWishesEdit = function (wish) {
      if (wish.granted)
        return;

      if (vm.myWishEditing && vm.myWishEditing._id !== wish._id) {
        vm.myWishesEditSave(vm.myWishEditing);
      }

      vm.myWishEditing = wish;
    };

    vm.myWishesRemove = function (wish) {
      Wishes.remove({ wishId: wish._id }, {}, function () {
        var index = vm.myWishes.findIndex(function (obj) {
          return wish._id === obj._id;
        });
        vm.myWishes.splice(index, 1);
      });
    };

    vm.myWishesEditSave = function (wish) {
      var wishService = new Wishes(wish);
      wishService.$update(function () {
        if (vm.myWishEditing._id === wish._id)
          vm.myWishEditing = null;
      });
    };

    vm.myWishesGrant = function (wish) {
      wish.granted = true;
      Wishes.granted({ wishId: wish._id }, {});
    };

    vm.loadProfile = function (user) {
      vm.profile = FriendshipsService.profile({ userId: user._id }, {});
    };

    vm.removeFriend = function (friend) {
      FriendshipsService.delete({ friendId: friend._id }, {}, function () {
        var index = vm.friends.findIndex(function (obj) {
          return friend._id === obj._id;
        });
        vm.friends.splice(index, 1);
      });
    };

    vm.addFriend = function (user) {
      FriendshipsService.save({ friend: user._id }, function () {
        var index = vm.friendsSearchResults.findIndex(function (obj) {
          return user._id === obj._id;
        });
        vm.friendsSearchResults.splice(index, 1);
        vm.friends.push(user);
      });
    };

    vm.search = function () {
      if (vm.searchQuery) {
        FriendshipsService.search({
          query: vm.searchQuery
        }, function (data) {
          var same = (vm.friendsSearchResults !== undefined);
          for (var i = 0; i < data.length && same; i++)
            same = data[i]._id === vm.friendsSearchResults[i];
          if (!same || data.length === 0)
            vm.friendsSearchResults = data;
        });
      } else {
        vm.friendsSearchResults = null;
      }
    };

    vm.wishReserve = function (wish) {
      Wishes.reserve({ wishId: wish._id }, {}, function () {
        wish.reserved = true;
        vm.reservedWishes.push(wish);
      });
    };

    vm.wishReplicate = function (wish) {
      Wishes.save({ name: wish.name, description: wish.description }, function (newWish) {
        vm.myWishes.push(newWish);
        vm.myWishesSort();
      });
    };

    vm.wishUnreserve = function (wish) {
      Wishes.unreserve({ wishId: wish._id }, {}, function () {
        wish.reserved = false;
        for (var i = 0; i < vm.profile.wishes.length; i++) {
          if (wish._id === vm.profile.wishes[i]._id) {
            vm.profile.wishes[i].reserved = false;
          }
        }
        wish.reserved = false;
        var index = vm.reservedWishes.findIndex(function (obj) {
          return wish._id === obj._id;
        });
        vm.reservedWishes.splice(index, 1);
      });
    };

    vm.signUp = function () {
      vm.signUpError = null;

      $http.post('/api/auth/signup', vm.signUpCredentials).success(function (response) {
        vm.user = response;
        vm.wishesToAdd = vm.myWishes;
        vm.initUser();
        var onSuccessCallback = function (newWish) {
          vm.myWishes.push(newWish);
          vm.myWishesSort();
        };
        while (vm.wishesToAdd.length > 0) {
          var wishToAdd = vm.wishesToAdd.pop();
          Wishes.save({ name: wishToAdd.name, description: wishToAdd.description }, onSuccessCallback);
        }
      }).error(function (response) {
        vm.signUpError = response.message;
      });
    };

    vm.signIn = function () {
      vm.signInError = null;

      $http.post('/api/auth/signin', vm.credentials).success(function (response) {
        vm.user = response;
        vm.wishesToAdd = vm.myWishes;
        vm.initUser();
        var onSuccessCallback = function (newWish) {
          vm.myWishes.push(newWish);
          vm.myWishesSort();
        };
        while (vm.wishesToAdd.length > 0) {
          var wishToAdd = vm.wishesToAdd.pop();
          Wishes.save({ name: wishToAdd.name, description: wishToAdd.description },onSuccessCallback);
        }
      }).error(function (response) {
        vm.signInError = response.message;
      });
    };

  }

})();
'use strict';

// Configuring the Articles module
angular.module('users.admin').run(['Menus',
  function (Menus) {
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Manage Users',
      state: 'admin.users'
    });
  }
]);

'use strict';

// Setting up route
angular.module('users.admin.routes').config(['$stateProvider',
  function ($stateProvider) {
    $stateProvider
      .state('admin.users', {
        url: '/users',
        templateUrl: 'modules/users/client/views/admin/list-users.client.view.html',
        controller: 'UserListController'
      })
      .state('admin.user', {
        url: '/users/:userId',
        templateUrl: 'modules/users/client/views/admin/view-user.client.view.html',
        controller: 'UserController',
        resolve: {
          userResolve: ['$stateParams', 'Admin', function ($stateParams, Admin) {
            return Admin.get({
              userId: $stateParams.userId
            });
          }]
        }
      })
      .state('admin.user-edit', {
        url: '/users/:userId/edit',
        templateUrl: 'modules/users/client/views/admin/edit-user.client.view.html',
        controller: 'UserController',
        resolve: {
          userResolve: ['$stateParams', 'Admin', function ($stateParams, Admin) {
            return Admin.get({
              userId: $stateParams.userId
            });
          }]
        }
      });
  }
]);

'use strict';

// Config HTTP Error Handling
angular.module('users').config(['$httpProvider',
  function ($httpProvider) {
    // Set the httpProvider "not authorized" interceptor
    $httpProvider.interceptors.push(['$q', '$location', 'Authentication',
      function ($q, $location, Authentication) {
        return {
          responseError: function (rejection) {
            switch (rejection.status) {
              case 401:
                // Deauthenticate the global user
                Authentication.user = null;

                // Redirect to signin page
                $location.path('signin');
                break;
              case 403:
                // Add unauthorized behaviour
                break;
            }

            return $q.reject(rejection);
          }
        };
      }
    ]);
  }
]);

'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
  function ($stateProvider) {
    // Users state routing
    $stateProvider
      .state('settings', {
        abstract: true,
        url: '/settings',
        templateUrl: 'modules/users/client/views/settings/settings.client.view.html',
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('settings.profile', {
        url: '/profile',
        templateUrl: 'modules/users/client/views/settings/edit-profile.client.view.html'
      })
      .state('settings.password', {
        url: '/password',
        templateUrl: 'modules/users/client/views/settings/change-password.client.view.html'
      })
      .state('settings.accounts', {
        url: '/accounts',
        templateUrl: 'modules/users/client/views/settings/manage-social-accounts.client.view.html'
      })
      .state('settings.picture', {
        url: '/picture',
        templateUrl: 'modules/users/client/views/settings/change-profile-picture.client.view.html'
      })
      .state('authentication', {
        abstract: true,
        url: '/authentication',
        templateUrl: 'modules/users/client/views/authentication/authentication.client.view.html'
      })
      .state('authentication.signup', {
        url: '/signup',
        templateUrl: 'modules/users/client/views/authentication/signup.client.view.html'
      })
      .state('authentication.signin', {
        url: '/signin?err',
        templateUrl: 'modules/users/client/views/authentication/signin.client.view.html'
      })
      .state('password', {
        abstract: true,
        url: '/password',
        template: '<ui-view/>'
      })
      .state('password.forgot', {
        url: '/forgot',
        templateUrl: 'modules/users/client/views/password/forgot-password.client.view.html'
      })
      .state('password.reset', {
        abstract: true,
        url: '/reset',
        template: '<ui-view/>'
      })
      .state('password.reset.invalid', {
        url: '/invalid',
        templateUrl: 'modules/users/client/views/password/reset-password-invalid.client.view.html'
      })
      .state('password.reset.success', {
        url: '/success',
        templateUrl: 'modules/users/client/views/password/reset-password-success.client.view.html'
      })
      .state('password.reset.form', {
        url: '/:token',
        templateUrl: 'modules/users/client/views/password/reset-password.client.view.html'
      });
  }
]);

'use strict';

angular.module('users.admin').controller('UserListController', ['$scope', '$filter', 'Admin',
  function ($scope, $filter, Admin) {
    Admin.query(function (data) {
      $scope.users = data;
      $scope.buildPager();
    });

    $scope.buildPager = function () {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 15;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };

    $scope.figureOutItemsToDisplay = function () {
      $scope.filteredItems = $filter('filter')($scope.users, {
        $: $scope.search
      });
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };

    $scope.pageChanged = function () {
      $scope.figureOutItemsToDisplay();
    };
  }
]);

'use strict';

angular.module('users.admin').controller('UserController', ['$scope', '$state', 'Authentication', 'userResolve',
  function ($scope, $state, Authentication, userResolve) {
    $scope.authentication = Authentication;
    $scope.user = userResolve;

    $scope.remove = function (user) {
      if (confirm('Are you sure you want to delete this user?')) {
        if (user) {
          user.$remove();

          $scope.users.splice($scope.users.indexOf(user), 1);
        } else {
          $scope.user.$remove(function () {
            $state.go('admin.users');
          });
        }
      }
    };

    $scope.update = function (isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'userForm');

        return false;
      }

      var user = $scope.user;

      user.$update(function () {
        $state.go('admin.user', {
          userId: user._id
        });
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
  }
]);

'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$state', '$http', '$location', '$window', 'Authentication', 'PasswordValidator',
  function ($scope, $state, $http, $location, $window, Authentication, PasswordValidator) {
    $scope.authentication = Authentication;
    $scope.popoverMsg = PasswordValidator.getPopoverMsg();

    // Get an eventual error defined in the URL query string:
    $scope.error = $location.search().err;

    // If user is signed in then redirect back home
    if ($scope.authentication.user) {
      $location.path('/');
    }

    $scope.signup = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'userForm');

        return false;
      }

      $http.post('/api/auth/signup', $scope.credentials).success(function (response) {
        // If successful we assign the response to the global user model
        $scope.authentication.user = response;

        // And redirect to the previous or home page
        $state.go($state.previous.state.name || 'home', $state.previous.params);
      }).error(function (response) {
        $scope.error = response.message;
      });
    };

    $scope.signin = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'userForm');

        return false;
      }

      $http.post('/api/auth/signin', $scope.credentials).success(function (response) {
        // If successful we assign the response to the global user model
        $scope.authentication.user = response;

        // And redirect to the previous or home page
        $state.go($state.previous.state.name || 'home', $state.previous.params);
      }).error(function (response) {
        $scope.error = response.message;
      });
    };

    // OAuth provider request
    $scope.callOauthProvider = function (url) {
      if ($state.previous && $state.previous.href) {
        url += '?redirect_to=' + encodeURIComponent($state.previous.href);
      }

      // Effectively call OAuth authentication route:
      $window.location.href = url;
    };
  }
]);

'use strict';

angular.module('users').controller('PasswordController', ['$scope', '$stateParams', '$http', '$location', 'Authentication', 'PasswordValidator',
  function ($scope, $stateParams, $http, $location, Authentication, PasswordValidator) {
    $scope.authentication = Authentication;
    $scope.popoverMsg = PasswordValidator.getPopoverMsg();

    //If user is signed in then redirect back home
    if ($scope.authentication.user) {
      $location.path('/');
    }

    // Submit forgotten password account id
    $scope.askForPasswordReset = function (isValid) {
      $scope.success = $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'forgotPasswordForm');

        return false;
      }

      $http.post('/api/auth/forgot', $scope.credentials).success(function (response) {
        // Show user success message and clear form
        $scope.credentials = null;
        $scope.success = response.message;

      }).error(function (response) {
        // Show user error message and clear form
        $scope.credentials = null;
        $scope.error = response.message;
      });
    };

    // Change user password
    $scope.resetUserPassword = function (isValid) {
      $scope.success = $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'resetPasswordForm');

        return false;
      }

      $http.post('/api/auth/reset/' + $stateParams.token, $scope.passwordDetails).success(function (response) {
        // If successful show success message and clear form
        $scope.passwordDetails = null;

        // Attach user profile
        Authentication.user = response;

        // And redirect to the index page
        $location.path('/password/reset/success');
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
  }
]);

'use strict';

angular.module('users').controller('ChangePasswordController', ['$scope', '$http', 'Authentication', 'PasswordValidator',
  function ($scope, $http, Authentication, PasswordValidator) {
    $scope.user = Authentication.user;
    $scope.popoverMsg = PasswordValidator.getPopoverMsg();

    // Change user password
    $scope.changeUserPassword = function (isValid) {
      $scope.success = $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'passwordForm');

        return false;
      }

      $http.post('/api/users/password', $scope.passwordDetails).success(function (response) {
        // If successful show success message and clear form
        $scope.$broadcast('show-errors-reset', 'passwordForm');
        $scope.success = true;
        $scope.passwordDetails = null;
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
  }
]);

'use strict';

angular.module('users').controller('ChangeProfilePictureController', ['$scope', '$timeout', '$window', 'Authentication', 'FileUploader',
  function ($scope, $timeout, $window, Authentication, FileUploader) {
    $scope.user = Authentication.user;
    $scope.imageURL = $scope.user.profileImageURL;

    // Create file uploader instance
    $scope.uploader = new FileUploader({
      url: 'api/users/picture',
      alias: 'newProfilePicture'
    });

    // Set file uploader image filter
    $scope.uploader.filters.push({
      name: 'imageFilter',
      fn: function (item, options) {
        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
      }
    });

    // Called after the user selected a new picture file
    $scope.uploader.onAfterAddingFile = function (fileItem) {
      if ($window.FileReader) {
        var fileReader = new FileReader();
        fileReader.readAsDataURL(fileItem._file);

        fileReader.onload = function (fileReaderEvent) {
          $timeout(function () {
            $scope.imageURL = fileReaderEvent.target.result;
          }, 0);
        };
      }
    };

    // Called after the user has successfully uploaded a new picture
    $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
      // Show success message
      $scope.success = true;

      // Populate user object
      $scope.user = Authentication.user = response;

      // Clear upload buttons
      $scope.cancelUpload();
    };

    // Called after the user has failed to uploaded a new picture
    $scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
      // Clear upload buttons
      $scope.cancelUpload();

      // Show error message
      $scope.error = response.message;
    };

    // Change user profile picture
    $scope.uploadProfilePicture = function () {
      // Clear messages
      $scope.success = $scope.error = null;

      // Start upload
      $scope.uploader.uploadAll();
    };

    // Cancel the upload process
    $scope.cancelUpload = function () {
      $scope.uploader.clearQueue();
      $scope.imageURL = $scope.user.profileImageURL;
    };
  }
]);

'use strict';

angular.module('users').controller('EditProfileController', ['$scope', '$http', '$location', 'Users', 'Authentication',
  function ($scope, $http, $location, Users, Authentication) {
    $scope.user = Authentication.user;

    // Update a user profile
    $scope.updateUserProfile = function (isValid) {
      $scope.success = $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'userForm');

        return false;
      }

      var user = new Users($scope.user);

      user.$update(function (response) {
        $scope.$broadcast('show-errors-reset', 'userForm');

        $scope.success = true;
        Authentication.user = response;
      }, function (response) {
        $scope.error = response.data.message;
      });
    };
  }
]);

'use strict';

angular.module('users').controller('SocialAccountsController', ['$scope', '$http', 'Authentication',
  function ($scope, $http, Authentication) {
    $scope.user = Authentication.user;

    // Check if there are additional accounts
    $scope.hasConnectedAdditionalSocialAccounts = function (provider) {
      for (var i in $scope.user.additionalProvidersData) {
        return true;
      }

      return false;
    };

    // Check if provider is already in use with current user
    $scope.isConnectedSocialAccount = function (provider) {
      return $scope.user.provider === provider || ($scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider]);
    };

    // Remove a user social account
    $scope.removeUserSocialAccount = function (provider) {
      $scope.success = $scope.error = null;

      $http.delete('/api/users/accounts', {
        params: {
          provider: provider
        }
      }).success(function (response) {
        // If successful show success message and clear form
        $scope.success = true;
        $scope.user = Authentication.user = response;
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
  }
]);

'use strict';

angular.module('users').controller('SettingsController', ['$scope', 'Authentication',
  function ($scope, Authentication) {
    $scope.user = Authentication.user;
  }
]);

'use strict';

angular.module('users')
  .directive('passwordValidator', ['PasswordValidator', function(PasswordValidator) {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, ngModel) {
        ngModel.$validators.requirements = function (password) {
          var status = true;
          if (password) {
            var result = PasswordValidator.getResult(password);
            var requirementsIdx = 0;

            // Requirements Meter - visual indicator for users
            var requirementsMeter = [
              { color: 'danger', progress: '20' },
              { color: 'warning', progress: '40' },
              { color: 'info', progress: '60' },
              { color: 'primary', progress: '80' },
              { color: 'success', progress: '100' }
            ];

            if (result.errors.length < requirementsMeter.length) {
              requirementsIdx = requirementsMeter.length - result.errors.length - 1;
            }

            scope.requirementsColor = requirementsMeter[requirementsIdx].color;
            scope.requirementsProgress = requirementsMeter[requirementsIdx].progress;

            if (result.errors.length) {
              scope.popoverMsg = PasswordValidator.getPopoverMsg();
              scope.passwordErrors = result.errors;
              status = false;
            } else {
              scope.popoverMsg = '';
              scope.passwordErrors = [];
              status = true;
            }
          }
          return status;
        };
      }
    };
  }]);

'use strict';

angular.module('users')
  .directive('passwordVerify', [function() {
    return {
      require: 'ngModel',
      scope: {
        passwordVerify: '='
      },
      link: function(scope, element, attrs, ngModel) {
        var status = true;
        scope.$watch(function() {
          var combined;
          if (scope.passwordVerify || ngModel) {
            combined = scope.passwordVerify + '_' + ngModel;
          }
          return combined;
        }, function(value) {
          if (value) {
            ngModel.$validators.passwordVerify = function (password) {
              var origin = scope.passwordVerify;
              return (origin !== password) ? false : true;
            };
          }
        });
      }
    };
  }]);

'use strict';

// Users directive used to force lowercase input
angular.module('users').directive('lowercase', function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, modelCtrl) {
      modelCtrl.$parsers.push(function (input) {
        return input ? input.toLowerCase() : '';
      });
      element.css('text-transform', 'lowercase');
    }
  };
});

'use strict';

// Authentication service for user variables
angular.module('users').factory('Authentication', ['$window',
  function ($window) {
    var auth = {
      user: $window.user
    };

    return auth;
  }
]);

'use strict';

// PasswordValidator service used for testing the password strength
angular.module('users').factory('PasswordValidator', ['$window',
  function ($window) {
    var owaspPasswordStrengthTest = $window.owaspPasswordStrengthTest;

    return {
      getResult: function (password) {
        var result = owaspPasswordStrengthTest.test(password);
        return result;
      },
      getPopoverMsg: function () {
        var popoverMsg = 'Please enter a passphrase or password with greater than 10 characters, numbers, lowercase, upppercase, and special characters.';
        return popoverMsg;
      }
    };
  }
]);

'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', ['$resource',
  function ($resource) {
    return $resource('api/users', {}, {
      update: {
        method: 'PUT'
      }
    });
  }
]);

//TODO this should be Users service
angular.module('users.admin').factory('Admin', ['$resource',
  function ($resource) {
    return $resource('api/users/:userId', {
      userId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);

(function () {
  'use strict';

  angular.module('wishes').run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    Menus.addMenuItem('topbar', {
      title: 'My wishes',
      state: 'wishes.my',
      icon: 'star',
      roles: ['user', 'admin']
    });

    Menus.addMenuItem('topbar', {
      title: 'Reserved wishes',
      state: 'wishes.reserved',
      icon: 'check',
      roles: ['user', 'admin']
    });
  }
}());
'use strict';

// Setting up route
angular.module('wishes').config(['$stateProvider',
  function ($stateProvider) {
    // Users state routing
    $stateProvider
      .state('wishes', {
        abstract: true,
        url: '/wishes',
        template: '<ui-view/>'
      })
      .state('wishes.my', {
        url: '/my',
        templateUrl: 'modules/wishes/client/views/my-wishes/list.client.view.html',
        controller: 'MyWishesController'
      })
      .state('wishes.new', {
        url: '/new',
        templateUrl: 'modules/wishes/client/views/my-wishes/edit.client.view.html',
        controller: 'EditWishesController',
        resolve: {
          wishResolve: ['Wishes', function (Wishes) {
            return new Wishes();
          }]
        }
      })
      .state('wishes.edit', {
        url: '/:wishId/edit',
        templateUrl: 'modules/wishes/client/views/my-wishes/edit.client.view.html',
        controller: 'EditWishesController',
        resolve: {
          wishResolve: ['$stateParams', 'Wishes', function ($stateParams, Wishes) {
            return Wishes.get({
              wishId: $stateParams.wishId
            }).$promise;
          }]
        }
      })
      .state('wishes.reserved', {
        url: '/reserved',
        templateUrl: 'modules/wishes/client/views/reserved-wishes/list.client.view.html',
        controller: 'ReservedWishesController',
        resolve: {
          wishResolve: ['Wishes', function (Wishes) {
            return Wishes.reserved().$promise;
          }]
        }
      });
  }
]);
'use strict';

angular.module('wishes').controller('EditWishesController', ['$scope', '$state', '$filter', 'Wishes', 'Authentication', 'wishResolve',
  function ($scope, $state, $filter, Wishes, Authentication, wish) {
    $scope.user = Authentication.user;
    $scope.wish = wish;

    // Update a wish
    $scope.updateWish = function (isValid) {
      $scope.success = $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'wishForm');

        return false;
      }

      var wish = new Wishes($scope.wish);

      if (wish._id) {
        wish.$update(successCallback, errorCallback);
      } else {
        wish.$save(successCallback, errorCallback);
      }

      function successCallback() {
        $scope.$broadcast('show-errors-reset', 'wishForm');

        $scope.success = true;

        $state.go('wishes.my');
      }

      function errorCallback(response) {
        $scope.error = response.data.message;
      }
    };
  }
]);

'use strict';

angular.module('wishes').controller('MyWishesController', ['$scope', '$filter', 'Wishes',
  function ($scope, $filter, Wishes) {
    $scope.allWishes = null;

    $scope.sortAllWishes = function () {
      $scope.wishes = [];
      $scope.grantedWishes = [];
      for (var i = 0; i < $scope.allWishes.length; i++) {
        if (!$scope.allWishes[i].granted) {
          $scope.wishes.push($scope.allWishes[i]);
        } else {
          $scope.grantedWishes.push($scope.allWishes[i]);
        }
      }
    };

    Wishes.query(function (data) {
      $scope.allWishes = data;
      $scope.sortAllWishes();
      $scope.buildPager();
    });

    $scope.buildPager = function () {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 15;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };

    $scope.figureOutItemsToDisplay = function () {
      $scope.filteredItems = $filter('filter')($scope.wishes, {
        $: $scope.search
      });
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };

    $scope.pageChanged = function () {
      $scope.figureOutItemsToDisplay();
    };

    $scope.delete = function (wish) {
      var wishService = new Wishes(wish);

      wishService.$delete(function () {
        $scope.success = true;

        var index = $scope.wishes.findIndex(function (obj) {
          return wish._id === obj._id;
        });
        $scope.wishes.splice(index, 1);
        $scope.pageChanged();
      }, function (response) {
        $scope.error = response.data.message;
      });
    };

    $scope.setGranted = function (wish) {
      Wishes.granted({ wishId: wish._id }, {}, function () {
        wish.granted = true;
        $scope.sortAllWishes();
        $scope.pageChanged();
      }, function (response) {
        $scope.error = response.data.message;
      });
    };
  }
]);

'use strict';

angular.module('wishes').controller('ReservedWishesController', ['$scope', 'wishResolve', 'Wishes',
  function ($scope, wishes, Wishes) {
    $scope.wishes = wishes;

    $scope.delete = function(wish) {

      Wishes.unreserve({ wishId: wish._id }, {}, function () {

        var index = $scope.wishes.findIndex(function (obj) {
          return wish._id === obj._id;
        });
        $scope.wishes.splice(index, 1);

      }, function (response) {
        $scope.error = response.data.message;
      });
    };
  }
]);

'use strict';

// Users directive used to force lowercase input
angular.module('wishes').directive('lowercase', function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, modelCtrl) {
      modelCtrl.$parsers.push(function (input) {
        return input ? input.toLowerCase() : '';
      });
      element.css('text-transform', 'lowercase');
    }
  };
});

'use strict';

// Wish service used for communicating with the wish REST endpoint
angular.module('wishes').factory('Wishes', ['$resource',
  function ($resource) {
    return $resource('api/wishes/:wishId', {
      wishId: '@_id'
    }, {
      update: {
        method: 'PUT'
      },
      granted: {
        method: 'POST',
        url: 'api/wishes/:wishId/granted'
      },
      reserve: {
        method: 'POST',
        url: 'api/wishes/:wishId/reserve'
      },
      unreserve: {
        method: 'DELETE',
        url: 'api/wishes/:wishId/reserve'
      },
      reserved: {
        method: 'GET',
        url: 'api/wishes/reserved/get',
        isArray: true
      }
    });
  }
]);