'use strict';

angular.module('wishes').controller('ReservedWishesController', ['$scope', 'wishResolve', 'Wishes',
  function ($scope, wishes, Wishes) {
    $scope.wishes = wishes;

    $scope.delete = function(wish) {

      Wishes.unreserve({ wishId: wish._id }, {}, function () {

        var index = $scope.wishes.findIndex(function (obj) {
          return wish._id === obj._id;
        });
        $scope.wishes.splice(index, 1);

      }, function (response) {
        $scope.error = response.data.message;
      });
    };
  }
]);
