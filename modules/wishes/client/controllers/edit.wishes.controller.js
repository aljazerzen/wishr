'use strict';

angular.module('wishes').controller('EditWishesController', ['$scope', '$state', '$filter', 'Wishes', 'Authentication', 'wishResolve',
  function ($scope, $state, $filter, Wishes, Authentication, wish) {
    $scope.user = Authentication.user;
    $scope.wish = wish;

    // Update a wish
    $scope.updateWish = function (isValid) {
      $scope.success = $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'wishForm');

        return false;
      }

      var wish = new Wishes($scope.wish);

      if (wish._id) {
        wish.$update(successCallback, errorCallback);
      } else {
        wish.$save(successCallback, errorCallback);
      }

      function successCallback() {
        $scope.$broadcast('show-errors-reset', 'wishForm');

        $scope.success = true;

        $state.go('wishes.my');
      }

      function errorCallback(response) {
        $scope.error = response.data.message;
      }
    };
  }
]);
