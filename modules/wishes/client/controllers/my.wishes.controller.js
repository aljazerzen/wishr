'use strict';

angular.module('wishes').controller('MyWishesController', ['$scope', '$filter', 'Wishes',
  function ($scope, $filter, Wishes) {
    $scope.allWishes = null;

    $scope.sortAllWishes = function () {
      $scope.wishes = [];
      $scope.grantedWishes = [];
      for (var i = 0; i < $scope.allWishes.length; i++) {
        if (!$scope.allWishes[i].granted) {
          $scope.wishes.push($scope.allWishes[i]);
        } else {
          $scope.grantedWishes.push($scope.allWishes[i]);
        }
      }
    };

    Wishes.query(function (data) {
      $scope.allWishes = data;
      $scope.sortAllWishes();
      $scope.buildPager();
    });

    $scope.buildPager = function () {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 15;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };

    $scope.figureOutItemsToDisplay = function () {
      $scope.filteredItems = $filter('filter')($scope.wishes, {
        $: $scope.search
      });
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };

    $scope.pageChanged = function () {
      $scope.figureOutItemsToDisplay();
    };

    $scope.delete = function (wish) {
      var wishService = new Wishes(wish);

      wishService.$delete(function () {
        $scope.success = true;

        var index = $scope.wishes.findIndex(function (obj) {
          return wish._id === obj._id;
        });
        $scope.wishes.splice(index, 1);
        $scope.pageChanged();
      }, function (response) {
        $scope.error = response.data.message;
      });
    };

    $scope.setGranted = function (wish) {
      Wishes.granted({ wishId: wish._id }, {}, function () {
        wish.granted = true;
        $scope.sortAllWishes();
        $scope.pageChanged();
      }, function (response) {
        $scope.error = response.data.message;
      });
    };
  }
]);
