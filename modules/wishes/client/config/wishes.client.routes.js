'use strict';

// Setting up route
angular.module('wishes').config(['$stateProvider',
  function ($stateProvider) {
    // Users state routing
    $stateProvider
      .state('wishes', {
        abstract: true,
        url: '/wishes',
        template: '<ui-view/>'
      })
      .state('wishes.my', {
        url: '/my',
        templateUrl: 'modules/wishes/client/views/my-wishes/list.client.view.html',
        controller: 'MyWishesController'
      })
      .state('wishes.new', {
        url: '/new',
        templateUrl: 'modules/wishes/client/views/my-wishes/edit.client.view.html',
        controller: 'EditWishesController',
        resolve: {
          wishResolve: ['Wishes', function (Wishes) {
            return new Wishes();
          }]
        }
      })
      .state('wishes.edit', {
        url: '/:wishId/edit',
        templateUrl: 'modules/wishes/client/views/my-wishes/edit.client.view.html',
        controller: 'EditWishesController',
        resolve: {
          wishResolve: ['$stateParams', 'Wishes', function ($stateParams, Wishes) {
            return Wishes.get({
              wishId: $stateParams.wishId
            }).$promise;
          }]
        }
      })
      .state('wishes.reserved', {
        url: '/reserved',
        templateUrl: 'modules/wishes/client/views/reserved-wishes/list.client.view.html',
        controller: 'ReservedWishesController',
        resolve: {
          wishResolve: ['Wishes', function (Wishes) {
            return Wishes.reserved().$promise;
          }]
        }
      });
  }
]);