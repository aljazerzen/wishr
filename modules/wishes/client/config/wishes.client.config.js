(function () {
  'use strict';

  angular.module('wishes').run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    Menus.addMenuItem('topbar', {
      title: 'My wishes',
      state: 'wishes.my',
      icon: 'star',
      roles: ['user', 'admin']
    });

    Menus.addMenuItem('topbar', {
      title: 'Reserved wishes',
      state: 'wishes.reserved',
      icon: 'check',
      roles: ['user', 'admin']
    });
  }
}());