'use strict';

// Wish service used for communicating with the wish REST endpoint
angular.module('wishes').factory('Wishes', ['$resource',
  function ($resource) {
    return $resource('api/wishes/:wishId', {
      wishId: '@_id'
    }, {
      update: {
        method: 'PUT'
      },
      granted: {
        method: 'POST',
        url: 'api/wishes/:wishId/granted'
      },
      reserve: {
        method: 'POST',
        url: 'api/wishes/:wishId/reserve'
      },
      unreserve: {
        method: 'DELETE',
        url: 'api/wishes/:wishId/reserve'
      },
      reserved: {
        method: 'GET',
        url: 'api/wishes/reserved/get',
        isArray: true
      }
    });
  }
]);