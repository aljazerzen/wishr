'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  crypto = require('crypto'),
  validator = require('validator');

var isNonEmpty = function (str) {
  return str.length > 0;
};

/**
 * User Schema
 */
var WishSchema = new Schema({
  name: {
    type: String,
    trim: true,
    validate: [isNonEmpty, 'Please fill in the name of your wish']
  },
  description: {
    type: String,
    trim: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: 'Invalid user'
  },
  exampleImageURL: {
    type: String,
    default: 'modules/wishes/client/img/example/default.png'
  },
  granted: {
    type: Boolean,
    default: false
  },
  reserved: {
    type: Boolean,
    default: false
  },
  reservedBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  updated: {
    type: Date
  },
  created: {
    type: Date,
    default: Date.now
  }
});

mongoose.model('Wish', WishSchema);
