'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
  mongoose = require('mongoose'),
  path = require('path'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  Wish = mongoose.model('Wish');

/**
 * List my wishes
 */
exports.list = function (req, res) {
  Wish.find({
    user: req.user.id,
    granted: { $ne: true }
  }).exec(function (err, wishes) {
    if (err) {
      res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }
    if (err || !wishes) {
      res.status(400).send({
        message: 'Failed to load your wishes.'
      });
    }

    res.json(wishes || null);
  });
};

exports.reserved = function (req, res) {
  Wish.find({
    reservedBy: req.user.id,
    granted: false
  }).exec(function (err, wishes) {
    if (err) {
      res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }
    if (err || !wishes) {
      res.status(400).send({
        message: 'Failed to load your reserved wishes.'
      });
    }

    res.json(wishes || null);
  });
};

/**
 * Get my wish details
 */
exports.get = function (req, res) {
  res.json(req.wish || null);
};

exports.delete = function (req, res) {

  if (req.wish.granted) {
    return res.status(400).json({ message: 'You can\'t delete a wis that has already been granted' });
  }

  req.wish.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      return res.status(200).send();
    }
  });
};

/**
 * Update wish details
 */
module.exports.update = function (req, res) {
  // Init Variables
  var wish = req.wish;

  wish = _.extend(wish, req.body);
  wish.user = req.user.id;
  wish.updated = Date.now();

  wish.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      return res.json(wish);
    }
  });
};

/**
 * Reserve wish
 */
module.exports.reserve = function (req, res) {
  if (req.wish.reserved) {
    return res.status(400).json({ message: 'Wish is already reserved' });
  }

  req.wish.reserved = true;
  req.wish.reservedBy = req.user._id;
  req.wish.save();
  return res.status(200).send();
};

/**
 * Unreserve wish
 */
module.exports.unreserve = function (req, res) {
  console.log(req.wish);
  if (!req.wish.reserved) {
    return res.status(400).json({ message: 'This wish is not even reserved' });
  }
  if (req.wish.reservedBy.toString() !== req.user._id.toString()) {
    return res.status(400).json({ message: 'This wish is not reserved by you' });
  }

  req.wish.reserved = false;
  req.wish.reservedBy = null;
  req.wish.save();
  return res.status(200).send();
};


/**
 * Set granted wish
 */
module.exports.setGranted = function (req, res) {
  req.wish.granted = true;
  req.wish.save();
  return res.status(200).send();
};

/**
 * Create new wish
 */
module.exports.create = function (req, res) {
  // Init Variables

  var wish = new Wish(req.body);

  wish.user = req.user.id;
  wish.updated = Date.now();

  wish.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      return res.json(wish);
    }
  });
};

/**
 * Wish middleware
 */
module.exports.wishByID = function (req, res, next, id) {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Invalid wish'
    });
  }

  Wish.findOne({
    _id: id
  }).exec(function (err, wish) {
    if (err) {
      return next(err);
    } else if (!wish) {
      return next(new Error('Failed to load wish ' + id));
    }

    req.wish = wish;
    next();
  });
};

/**
 * Does user own this wish?
 */
exports.ownesThisWish = function (req, res, next) {
  if (req.wish.user.toString() !== req.user._id.toString()) {
    return res.status(400).json({ message: 'You do not own this wish.' });
  }
  next();
};

/**
 * Is user not owner if this wish?
 */
exports.doesNotOwnThisWish = function (req, res, next) {
  if (req.wish.user.toString() === req.user._id.toString()) {
    return res.status(400).json({ message: 'You can\' perform this action on your own wish.' });
  }
  next();
};

/**
 * Is user signed in?
 */
exports.signedIn = function (req, res, next) {
  // If signed in
  if (!req.user) {
    return res.status(400).json({ message: 'You must be signed in to do that' });
  }
  next();
};