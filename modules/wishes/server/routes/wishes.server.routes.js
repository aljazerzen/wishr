'use strict';

module.exports = function (app) {
  // User Routes
  var wishes = require('../controllers/wishes.server.controller');

  // Setting up the wishes profile api
  app.route('/api/wishes').all(wishes.signedIn)
    .get(wishes.list)
    .post(wishes.create);

  app.route('/api/wishes/reserved/get').all(wishes.signedIn)
    .get(wishes.reserved);

  app.route('/api/wishes/:wishId').all(wishes.signedIn).put(wishes.ownesThisWish)
    .get(wishes.get)
    .put(wishes.update)
    .delete(wishes.delete);

  app.route('/api/wishes/:wishId/reserve')
    .all(wishes.signedIn).all(wishes.doesNotOwnThisWish)
    .post(wishes.reserve)
    .delete(wishes.unreserve);

  app.route('/api/wishes/:wishId/granted')
    .post(wishes.signedIn).post(wishes.ownesThisWish).post(wishes.setGranted);

  // app.route('/api/wishes/picture').post(wishes.changeExamplePicture);

  // Finish by binding the user middleware
  app.param('wishId', wishes.wishByID);
};