'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Wish = mongoose.model('Wish'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create a user as a friend
 */
exports.create = function (req, res) {
  if (req.user._id === req.body.friend) {
    return res.status(400).send({
      message: 'You can\'t add yourself as a friend.'
    });
  }

  req.user.friends.push(req.body.friend);

  req.user.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(req.user);
    }
  });
};

/**
 * Show the profile of an user
 */
exports.read = function (req, res) {
  User.populate(req.profile, [
    { path: 'friends', select: 'displayName profileImageURL email' }
  ], function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {

      Wish.find({
        user: req.profile._id,
        granted: { $ne: true }
      }).select('name description exampleImageURL reserved').exec(function (err, wishes) {
        if (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        } else {

          var profile = {
            user: req.profile,
            wishes: wishes,
            isOwnProfile: req.user && req.profile._id.toString() === req.user._id.toString()
          };

          return res.jsonp(profile);
        }
      });
    }
  });
};

/**
 * Delete a friendship
 */
exports.delete = function (req, res) {
  req.user.friends.splice(req.friendIndex, 1);

  req.user.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.status(200).send();
    }
  });
};

/**
 * List of my friendships
 */
exports.list = function (req, res) {
  User.populate(req.user, { path: 'friends', select: 'displayName email' }, function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      return res.jsonp(req.user.friends);
    }
  });
};

/**
 * Search for users by name
 */
exports.search = function (req, res) {
  User.find({
    displayName: { $regex: req.params.query, $options: 'i' },
    _id: { $nin: req.user.friends, $ne: req.user._id }
  }).sort('-created').exec(function (err, users) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(users);
    }
  });
};

/**
 * Friend middleware
 */
exports.friendIndexByUserId = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'User is invalid'
    });
  }

  var index = req.user.friends.indexOf(id);

  if (index === -1) {
    return res.status(404).send({
      message: 'You are not friend with that user'
    });
  }

  req.friendIndex = index;
  next();
};
