'use strict';

/**
 * Module dependencies
 */
var friendshipsPolicy = require('../policies/friendships.server.policy'),
  friendships = require('../controllers/friendships.server.controller');

module.exports = function(app) {
  // Friendships Routes
  app.route('/api/friendships').all(friendshipsPolicy.isAllowed)
    .get(friendships.list)
    .post(friendships.create);

  app.route('/api/friendships/:friendId').all(friendshipsPolicy.isAllowed)
    .delete(friendships.delete);

  app.route('/api/users/:userId/profile')
    .get(friendships.read);

  app.route('/api/friendships/search/:query')
    .get(friendships.search);

  // Finish by binding the Friendship middleware
  app.param('friendId', friendships.friendIndexByUserId);
};
