(function () {
  'use strict';

  angular
    .module('friendships')
    .controller('FriendsSearchController', FriendsSearchController);

  FriendsSearchController.$inject = ['queryResolve', '$stateParams', 'FriendshipsService', '$state'];

  function FriendsSearchController(queryResults, $stateParams, FriendshipsService, $state) {
    var vm = this;

    vm.title = 'Search results';
    vm.empty = 'No results.';
    vm.searchQuery = $stateParams.query;
    vm.isSearchResult = true;

    vm.users = queryResults;

    vm.clear = function () {
      $state.go('friends.list');
    };

    vm.onChange = function () {
      if (vm.searchQuery) {
        vm.users = FriendshipsService.search({
          query: vm.searchQuery
        });
      }
    };

    vm.add = function (user) {
      vm.error = '';
      FriendshipsService.save({ friend: user._id }, function () {
        var index = vm.users.findIndex(function (obj) {
          return user._id === obj._id;
        });
        vm.users.splice(index, 1);
      }, function (response) {
        vm.error = response.data.message;
      });
    };
  }
}());
