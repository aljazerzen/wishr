(function () {
  'use strict';

  angular
    .module('friendships')
    .controller('ProfileController', ProfileController);

  ProfileController.$inject = ['FriendshipsService', '$state', 'userResolve', 'Wishes'];

  function ProfileController(FriendshipsService, $state, profile, Wishes) {
    var vm = this;

    vm.profile = profile;

    vm.reserve = function (wish) {
      Wishes.reserve({ wishId: wish._id }, {}, function () {
        wish.reserved = true;
      });
    };

    vm.addToMyList = function (wish) {
      var newWish = { name: wish.name, description: wish.description };
      Wishes.save({}, newWish, function () {
        $state.go('wishes.my');
      }, function () {
        // error
      });
    };

    vm.shouldShowFollowButton = function (friend) {
      if (friend._id !== window.user._id)
        return false;

      return window.user.friends.indexOf(friend) !== -1;
    };
  }
}());
