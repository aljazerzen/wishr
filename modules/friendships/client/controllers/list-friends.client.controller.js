(function () {
  'use strict';

  angular
    .module('friendships')
    .controller('FriendsListController', FriendsListController);

  FriendsListController.$inject = ['FriendshipsService', '$state'];

  function FriendsListController(FriendshipsService, $state) {
    var vm = this;

    vm.title = 'Friends';
    vm.empty = 'No friends jet.';
    vm.isSearchResult = false;

    vm.users = FriendshipsService.query();

    vm.search = function () {
      $state.go('friends.search', {
        query: vm.searchQuery
      });
    };

    vm.remove = function (user) {
      FriendshipsService.delete({
        friendId: user._id
      }, function () {
        var index = vm.users.findIndex(function (obj) {
          return user._id === obj._id;
        });
        vm.users.splice(index, 1);
      });
    };
  }
}());
