(function () {
  'use strict';

  angular
    .module('friendships')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Set top bar menu items
    Menus.addMenuItem('topbar', {
      title: 'Friends',
      state: 'friends.list',
      icon: 'users',
      roles: ['user', 'admin']
    });
  }
}());
