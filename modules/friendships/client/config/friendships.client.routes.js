'use strict';

angular.module('friendships').config(routeConfig);

routeConfig.$inject = ['$stateProvider'];

function routeConfig($stateProvider) {
  $stateProvider
    .state('friends', {
      abstract: true,
      url: '/friends',
      template: '<ui-view/>'
    })
    .state('friends.list', {
      url: '',
      templateUrl: 'modules/friendships/client/views/list-friendships.client.view.html',
      controller: 'FriendsListController',
      controllerAs: 'vm'
    })
    .state('friends.search', {
      url: '/search/:query',
      templateUrl: 'modules/friendships/client/views/list-friendships.client.view.html',
      controller: 'FriendsSearchController',
      controllerAs: 'vm',
      data: {
        pageTitle: 'Friends'
      },
      resolve: {
        queryResolve: ['$stateParams', 'FriendshipsService', function ($stateParams, FriendshipsService) {
          return FriendshipsService.search({
            query: $stateParams.query
          }).$promise;
        }]
      }
    })
    .state('user', {
      url: '/user/:userId',
      templateUrl: 'modules/friendships/client/views/profile.client.view.html',
      controller: 'ProfileController',
      controllerAs: 'vm',
      resolve: {
        userResolve: ['$stateParams', 'FriendshipsService', function ($stateParams, FriendshipsService) {
          return FriendshipsService.profile({
            userId: $stateParams.userId
          }).$promise;
        }]
      }
    });
}
