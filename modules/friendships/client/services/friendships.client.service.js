'use strict';

angular
  .module('friendships')
  .factory('FriendshipsService', FriendshipsService);

FriendshipsService.$inject = ['$resource'];

function FriendshipsService($resource) {
  return $resource('api/friendships/:friendId', {
    friendId: '@_id'
  }, {
    search: {
      url: '/api/friendships/search/:query',
      method: 'GET',
      isArray: true
    },
    profile: {
      url: '/api/users/:userId/profile',
      method: 'GET'
    }
  });
}