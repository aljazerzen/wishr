(function (app) {
  'use strict';

  app.registerModule('friendships', ['users', 'wishes']);
}(ApplicationConfiguration));
