(function () {
  'use strict';

  angular
    .module('front')
    .controller('FrontController', FrontController);

  FrontController.$inject = ['Wishes', 'FriendshipsService', '$http'];

  function FrontController(Wishes, FriendshipsService, $http) {
    var vm = this;

    vm.user = window.user || undefined;

    vm.title = 'Wishr';

    vm.initUser = function () {
      Wishes.query(function (wishes) {
        vm.myWishes = wishes;
        vm.myWishesSort();
      });

      Wishes.reserved(function (wishes) {
        vm.reservedWishes = wishes;
      });

      vm.friends = FriendshipsService.query();
    };

    if (vm.user) {
      vm.initUser();
    } else {
      vm.myWishes = [];
    }

    vm.myWishesAdd = function () {
      if (!vm.user) {
        vm.newWish._id = +new Date(); // unique id
        vm.myWishes.push(vm.newWish);
        vm.newWish = {};
        vm.myWishesSort();
      } else {
        Wishes.save(vm.newWish, function (newWish) {
          vm.myWishes.push(newWish);
          vm.myWishesSort();
        });
      }
    };

    vm.myWishesSort = function () {
      vm.myWishes.sort(function (wish1, wish2) {
        return wish1.granted > wish2.granted;
      });
    };

    vm.myWishesEdit = function (wish) {
      if (wish.granted)
        return;

      if (vm.myWishEditing && vm.myWishEditing._id !== wish._id) {
        vm.myWishesEditSave(vm.myWishEditing);
      }

      vm.myWishEditing = wish;
    };

    vm.myWishesRemove = function (wish) {
      Wishes.remove({ wishId: wish._id }, {}, function () {
        var index = vm.myWishes.findIndex(function (obj) {
          return wish._id === obj._id;
        });
        vm.myWishes.splice(index, 1);
      });
    };

    vm.myWishesEditSave = function (wish) {
      var wishService = new Wishes(wish);
      wishService.$update(function () {
        if (vm.myWishEditing._id === wish._id)
          vm.myWishEditing = null;
      });
    };

    vm.myWishesGrant = function (wish) {
      wish.granted = true;
      Wishes.granted({ wishId: wish._id }, {});
    };

    vm.loadProfile = function (user) {
      vm.profile = FriendshipsService.profile({ userId: user._id }, {});
    };

    vm.removeFriend = function (friend) {
      FriendshipsService.delete({ friendId: friend._id }, {}, function () {
        var index = vm.friends.findIndex(function (obj) {
          return friend._id === obj._id;
        });
        vm.friends.splice(index, 1);
      });
    };

    vm.addFriend = function (user) {
      FriendshipsService.save({ friend: user._id }, function () {
        var index = vm.friendsSearchResults.findIndex(function (obj) {
          return user._id === obj._id;
        });
        vm.friendsSearchResults.splice(index, 1);
        vm.friends.push(user);
      });
    };

    vm.search = function () {
      if (vm.searchQuery) {
        FriendshipsService.search({
          query: vm.searchQuery
        }, function (data) {
          var same = (vm.friendsSearchResults !== undefined);
          for (var i = 0; i < data.length && same; i++)
            same = data[i]._id === vm.friendsSearchResults[i];
          if (!same || data.length === 0)
            vm.friendsSearchResults = data;
        });
      } else {
        vm.friendsSearchResults = null;
      }
    };

    vm.wishReserve = function (wish) {
      Wishes.reserve({ wishId: wish._id }, {}, function () {
        wish.reserved = true;
        vm.reservedWishes.push(wish);
      });
    };

    vm.wishReplicate = function (wish) {
      Wishes.save({ name: wish.name, description: wish.description }, function (newWish) {
        vm.myWishes.push(newWish);
        vm.myWishesSort();
      });
    };

    vm.wishUnreserve = function (wish) {
      Wishes.unreserve({ wishId: wish._id }, {}, function () {
        wish.reserved = false;
        for (var i = 0; i < vm.profile.wishes.length; i++) {
          if (wish._id === vm.profile.wishes[i]._id) {
            vm.profile.wishes[i].reserved = false;
          }
        }
        wish.reserved = false;
        var index = vm.reservedWishes.findIndex(function (obj) {
          return wish._id === obj._id;
        });
        vm.reservedWishes.splice(index, 1);
      });
    };

    vm.signUp = function () {
      vm.signUpError = null;

      $http.post('/api/auth/signup', vm.signUpCredentials).success(function (response) {
        vm.user = response;
        vm.wishesToAdd = vm.myWishes;
        vm.initUser();
        var onSuccessCallback = function (newWish) {
          vm.myWishes.push(newWish);
          vm.myWishesSort();
        };
        while (vm.wishesToAdd.length > 0) {
          var wishToAdd = vm.wishesToAdd.pop();
          Wishes.save({ name: wishToAdd.name, description: wishToAdd.description }, onSuccessCallback);
        }
      }).error(function (response) {
        vm.signUpError = response.message;
      });
    };

    vm.signIn = function () {
      vm.signInError = null;

      $http.post('/api/auth/signin', vm.credentials).success(function (response) {
        vm.user = response;
        vm.wishesToAdd = vm.myWishes;
        vm.initUser();
        var onSuccessCallback = function (newWish) {
          vm.myWishes.push(newWish);
          vm.myWishesSort();
        };
        while (vm.wishesToAdd.length > 0) {
          var wishToAdd = vm.wishesToAdd.pop();
          Wishes.save({ name: wishToAdd.name, description: wishToAdd.description },onSuccessCallback);
        }
      }).error(function (response) {
        vm.signInError = response.message;
      });
    };

  }

})();