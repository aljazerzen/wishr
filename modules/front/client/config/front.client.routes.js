(function () {
  'use strict';

  angular
    .module('front')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('front', {
        url: '/front',
        templateUrl: '/modules/front/client/views/home.client.view.html',
        controller: 'FrontController',
        controllerAs: 'vm'
      });
  }

}());
