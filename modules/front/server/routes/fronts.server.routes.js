'use strict';

/**
 * Module dependencies
 */
var front = require('../controllers/front.server.controller');

module.exports = function(app) {
  // Fronts Routes
  app.route('/front').get(front.renderIndex);
};
