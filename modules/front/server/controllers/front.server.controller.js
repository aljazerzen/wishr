'use strict';

/**
 * Module dependencies.
 */
var path = require('path');

/**
 * Render the front application page
 */
exports.renderIndex = function (req, res) {
  res.render('modules/front/server/views/front', {
    user: req.user || null
  });
};
